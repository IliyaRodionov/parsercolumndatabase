<?php
/**
 * Created by PhpStorm.
 * User: Илия
 * Date: 07.11.2018
 * Time: 21:19
 */
//борьба с дублями и сохранение валидной информации
//анализ таблицы в которой присутствуют повторяющиеся строки(в нашем случае строки с одинаковыми артикулами)
//попытка собрать наиболее валидные данные из n-ного количества повторяющихся строк в одну

function articleFilter($article){

    $article = strtolower(trim($article));
    $search = ["-", " ", "/", "//", ":", ";", "!", "?", "'", "\"", "(", ")", ".", ",", "*"];
    $replace = "";
    $article = str_replace($search, $replace, $article);
    $article = stripcslashes($article);

    //$article = iconv(mb_detect_encoding($article),'cp1251',$article);
    //$article = mb_convert_encoding($article, "windows-1251");

    return $article;

}
function mb_ucfirst($text) {
    $text = trim($text, "\"");
    return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
}

$count = 0; //счетчик строк
$array_keyF0_and_keyF1 = array();
$array_bl_code_table = array();
$array_column = array();
$lost_article_encoding = array();
$lost_article_proiz_or_article = array();
$db_local = [
    'host'      => '127.0.0.1',
    'username'     => '',
    'password'  => '',
    'dbname'    => 'stozap',
    'port'      => '3306',
    'charset'   => 'utf8'
];
ini_set("memory_limit", -1);
$dsn = sprintf('mysql:host=%s:%s;dbname=%s;charset=%s',
    $db_local['host'],
    $db_local['port'],
    $db_local['dbname'],
    $db_local['charset']
);
try {
    $pdo = new PDO($dsn, $db_local['username'], $db_local['password']);

} catch (PDOException $e) {

    echo "error!: " . $e->getMessage() . "<br>";
    echo "\n"."Возможно такая база данных не существует или указан ошибочный порт! Обратитесь в службу поддержки!";
    die();

}

$data_article = file("C:\Bitnami\wampstack-5.6.30-3\apache2\htdocs\idrp\settings\stozap\article_info_new.txt",FILE_SKIP_EMPTY_LINES);

foreach ($data_article as $key => $value){
    $value = trim($value);
    $data_article[$key] = "'{$value}'";
}

$CID_string = implode(',', $data_article);

//IN($CID_string) 94764bc6618c6f8986095e976b0183c5
$query_select_bl_code_table = "SELECT F0, F1, CID FROM bl_code where `CID` IN($CID_string)";

$dbObject_bl_code_table = $pdo->prepare($query_select_bl_code_table);
$dbObject_bl_code_table->execute();

while ($result_bl_code = $dbObject_bl_code_table->fetch(PDO::FETCH_ASSOC)){

    $count++;

    foreach ($result_bl_code as $key => $value) {

        $array_keyF0_and_keyF1[$key] = $value;

    }

    $array_F0 = explode("\t", $array_keyF0_and_keyF1['F0']);
    $array_F1 = explode("\t", $array_keyF0_and_keyF1['F1']);

    foreach ($array_F0 as $key => $value){
        $array_F0[$key] = mb_ucfirst($value);
        //массив, который содержит индексы дублирующихся значений
        $countArr = array_keys($array_F0, $value);
        //удаляем все дубли, оставляем только первое значение в массиве $arrayF0 и $arrayF1(чтобы массивы были одинаковой длины и с валидными сопоставлениями)
        if(count($countArr) > 1){
            $count_Arr = count($countArr);
           // echo $count_Arr;
            while($count_Arr > 1){
                //удаляем дублирующее зачение из массива столбцов
                array_splice($array_F0, $countArr[$count_Arr-1], 1);
                //удаляем тоже значение(тот же индекс), в массиве значений этих столбцов
                if(array_key_exists($countArr[$count_Arr-1], $array_F1)) {
                    array_splice($array_F1, $countArr[$count_Arr - 1], 1);
                }
                $count_Arr--;
            }
        }

    }

    $CID_lost_article = $array_keyF0_and_keyF1['CID'];
    //смотрим какие столбцы есть в таблице, чтобы динамически добавить столбцы если они будут отсутствовать
    $query_column_data = "SELECT `COLUMN_NAME` 
                          FROM `INFORMATION_SCHEMA`.`COLUMNS` 
                          WHERE `TABLE_SCHEMA`='stozap'
                          AND `TABLE_NAME`='new_bl_search';";
    $dbObject_column_data = $pdo->prepare($query_column_data);
    $dbObject_column_data->execute();

    while($result_column_table = $dbObject_column_data->fetch(PDO::FETCH_ASSOC)){

        foreach ($result_column_table as $value){

            $array_column[] = $value;

        }

    }


    foreach ($array_F0 as $key => $value){
        $value = trim($value,".,\"");
        //если в массиве столбцов нашей таблицы отсутствует значение value, то добавляем новый столбец

        if(!in_array($value,$array_column) && $array_F0[0] == "Артикул"){

            $query_alter_column = "ALTER TABLE new_bl_search ADD `$value` varchar(255) DEFAULT NULL;";
            $ds_alter = $pdo->prepare($query_alter_column);
            $ds_alter->execute();

        }

    }

    $article = "";
    $proiz = "";
    $image = "_mod_files/ce_images/no_image.jpg";

    //подготовка данных для довабления в таблицу
    foreach ($array_F0 as $key => $value){

        if ($value == "Артикул"){
            //очищаем артикул от сторонних символов
            $array_F1[$key] = articleFilter($array_F1[$key]);
            $article = $array_F1[$key];

        }

        if ($value == "Изображение" && empty($array_F1[$key])) $array_F1[$key] = $image;

        if ($value == "Производитель" && !empty($array_F1[$key])) $proiz = $array_F1[$key];

        if($value == "Отверстия" && $array_F1[$key] == 0) $array_F1[$key] = 'Нет отверстий';
        //если значение для столбца отсутствует, выставляем дефолтный NULL
        if(empty($array_F1[$key])) $array_F1[$key] = "NULL";
        //обрамляем ключи в `` кавычки, чтобы они прошли через sql-запросы
        $value = trim($value,".,\"");
        $array_F0[$key] = "`{$value}`";

    }

    foreach ($array_F1 as $key => $value){
        $value = str_replace("'", "", $value);
        $array_F1[$key] = "'{$value}'";
    }
    //Название столбцов(для вставки в запрос)
    $str_ColumnTable_F0 = implode(",", $array_F0);
    //Значения столбцов(для вставки в запрос)
    $str_ColumnValue_F1 = implode(",", $array_F1);

    $coding = mb_detect_encoding($article);

    if (!empty($article) && !empty($proiz) && $coding !== "UTF-8") {
        //Проверка на существование данного артикула в таблице
        $query_existenceArticle = "SELECT EXISTS(SELECT id FROM stozap.new_bl_search WHERE `Артикул` = ?);";
        $dbObject_search_article = $pdo->prepare($query_existenceArticle);
        $dbObject_search_article->execute(array($article));
        $existenceArticle = $dbObject_search_article->fetchColumn();

        if($existenceArticle){
                //Проверка по изображению
                $query_search_image = "SELECT `Изображение` FROM stozap.new_bl_search WHERE `Артикул` = ?;";
                $dbObject_column_image = $pdo->prepare($query_search_image);
                $dbObject_column_image->execute(array($article));
                $result_column_image = $dbObject_column_image->fetch(PDO::FETCH_ASSOC);
                if($result_column_image['Изображение'] == NULL or $result_column_image['Изображение'] == $image){
                    //формируем массив ключ значение
                    $array_F0_F1_for_update_sql = array_combine($array_F0,$array_F1);
                    $update_string = "";
                    //формируем строку для запроса UPDATE в формате ключ = значение,...,ключ = значение
                    foreach ($array_F0_F1_for_update_sql as $key => $value){

                        $update_string.= $key."=".$value.",";

                    }

                    $update_string = (trim($update_string,","));

                    $query_update_string ="UPDATE stozap.new_bl_search SET $update_string WHERE `Артикул` = ?";
                    $query_update = $pdo->prepare($query_update_string);

                    $query_update->execute(array($article));
                    echo "Данная строка с артикулом = ".$article." обновлена по столбцу (Изображение)"."\n";

                } else {
                    //Проверка остальных нулевых столбцов

                    //запрашиваем строчку из таблицы, чтобы сравнить её с $array_F0_F1_for_update_sql;если в строчке будут присутствовать
                    //нулевые значения, то есть вероятность что мы их заполним акутальными значениями.(в контексте одного и того же артикула)
                    //борьба с дублями и сохранение валидной информации
                    $query_chek_column_of_NULL = "SELECT * FROM stozap.new_bl_search WHERE `Артикул` = ?";
                    $check_of_NULL = $pdo->prepare($query_chek_column_of_NULL);
                    $check_of_NULL->execute(array($article));
                    $result_check_of_NULL = $check_of_NULL->fetch(PDO::FETCH_ASSOC);

                    //формируем массив ключ=>значение для формирования строки запрося для update;
                    //arrayF0 и arrayF1 массивы, из новой итерации, из таблицы, которую парсим.(новая строчка)
                    $array_F0_F1_for_update_sql_for_NULL_column = array_combine($array_F0,$array_F1);
                    $update_string = "";
                   //проверяем пустые значения столбцов в нашей строке, если находим, то обновляем.(key - название столбца,value - значение столбца)
                    foreach ($result_check_of_NULL as $key => $value){

                        if(!isset($value) or $value ==  'NULL'){
                            $key = "`{$key}`";

                            if(array_key_exists($key, $array_F0_F1_for_update_sql_for_NULL_column)){

                                $update_string.= $key."=".$array_F0_F1_for_update_sql_for_NULL_column[$key].",";

                            }

                        }
                    }

                    $update_string = (trim($update_string,","));
                    $query_update_string ="UPDATE stozap.new_bl_search SET $update_string WHERE `Артикул` = ?";
                    $query_update = $pdo->prepare($query_update_string);
                    $query_update->execute(array($article));
                    echo "Данная строка с артикулом = ".$article." уже имеет актуальное изображение и обновлена по другим столбцам"."\n";

                }


            } else {

                $query_insertValue = "INSERT INTO stozap.new_bl_search(".$str_ColumnTable_F0.") VALUES(".$str_ColumnValue_F1.");";
                $ds_insert = $pdo->prepare($query_insertValue);
                $ds_insert->execute();
                echo "Строка ".$count." с артикулом ".$article."добавлена в таблицу\n";

            }

    } else {
        if($coding == "UTF-8") {
            if (!in_array($CID_lost_article, $lost_article_encoding)) {
                $lost_article_encoding[] = $CID_lost_article . "\n";
            }
        } else {
            if(!in_array($CID_lost_article, $lost_article_proiz_or_article)){
                $lost_article_proiz_or_article[] = $CID_lost_article . "\n";
            }
        }
        echo "Строка ".$count." обработана, артикул ".$article." не добавлен в таблицу из-за отсутствия артикула или производителя, или сбитой кодировки\n";

    }
}

$file_encoding = "article_lost_for_encoding.txt";
$file_proiz_or_article = "article_lot_for_proiz.txt";
file_put_contents($file_encoding, $lost_article_encoding);
file_put_contents($file_proiz_or_article, $lost_article_proiz_or_article);